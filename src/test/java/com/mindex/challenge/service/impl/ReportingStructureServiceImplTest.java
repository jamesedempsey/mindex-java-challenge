package com.mindex.challenge.service.impl;

import java.util.ArrayList;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.ReportingStructureService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReportingStructureServiceImplTest {

    private String reportingStructureWithIdUrl;

    @Autowired
    private ReportingStructureService reportingStructureService;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    
    @Before
    public void setup() {
        reportingStructureWithIdUrl = "http://localhost:" + port + "/reportingstructure/{employeeId}";
    }

    @Test
    public void testRead() {
        // Arrange 
        Employee testEmployee = new Employee();
        String testEmployeeId = "c0c2293d-16bd-4603-8e08-638a9d18b22c";
        testEmployee.setEmployeeId(testEmployeeId);
        testEmployee.setFirstName("George");
        testEmployee.setLastName("Harrison");
        testEmployee.setPosition("Developer III");
        testEmployee.setDepartment("Engineering");
        testEmployee.setDirectReports(new ArrayList<Employee>());
        
        ReportingStructure testReportingStructure = new ReportingStructure();
        testReportingStructure.setEmployee(testEmployee);
        testReportingStructure.setNumberOfReports(0);

        // Act on the ReportingStructure service
        // As the ReportingStructure service depends on the bootstrapped db contents/Employee Repo,
        // this test introduces coupling. Further improvements here could be made to break this coupling
        ReportingStructure readReportingStructure = restTemplate.getForEntity(reportingStructureWithIdUrl, ReportingStructure.class, testEmployeeId).getBody();

        // Assert
        assertReportingStructureEquivalence(testReportingStructure, readReportingStructure);
    }

    private static void assertReportingStructureEquivalence(ReportingStructure expected, ReportingStructure actual) {
        assertEquals(expected.getNumberOfReports(), actual.getNumberOfReports());
        assertEmployeeEquivalence(expected.getEmployee(), actual.getEmployee());
    }

    private static void assertEmployeeEquivalence(Employee expected, Employee actual) {
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getDepartment(), actual.getDepartment());
        assertEquals(expected.getPosition(), actual.getPosition());
    }
}
