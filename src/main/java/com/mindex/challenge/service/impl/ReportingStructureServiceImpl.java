package com.mindex.challenge.service.impl;

import java.util.ArrayList;
import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.ReportingStructureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportingStructureServiceImpl implements ReportingStructureService {
    private static final Logger LOG = LoggerFactory.getLogger(ReportingStructureServiceImpl.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public ReportingStructure read(String employeeId) {
        LOG.debug("Creating current reporting structure for employeeId [{}]", employeeId);

        // Root employee to create the reporting structure for
        Employee employee = employeeRepository.findByEmployeeId(employeeId);

        if (employee == null) {
            throw new RuntimeException("Invalid employeeId: " + employeeId);
        }
        
        // Modify the List contents with subsequent employee repo calls
        this.populateEmployeeContents(employee);

        ReportingStructure employeeReportingStructure = new ReportingStructure(employee);
        return employeeReportingStructure;
    }

    private Employee populateEmployeeContents(Employee employee) {
        
        ArrayList<Employee> reportList = new ArrayList<Employee>();
        
        if (employee.getDirectReports() != null) {
            for (Employee e : employee.getDirectReports()) {
                Employee populatedEmployee = employeeRepository.findByEmployeeId(e.getEmployeeId());
                reportList.add(populatedEmployee);
                this.populateEmployeeContents(populatedEmployee);
            }
        }
        employee.setDirectReports(reportList);
        return employee;
    }
}
