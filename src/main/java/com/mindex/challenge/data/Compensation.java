package com.mindex.challenge.data;

import java.util.Date;

/**
 * The Compensation class maintains three properties: an associated employee, an effective date,
 * and the employee's salary stored as a String. 
 */

public class Compensation {
    
    private Date effectiveDate;
    private Employee employee;
    private String salary;

    public Compensation() {
    }

    public Compensation(Employee employee, String salary, Date effectiveDate) {
        this.employee = employee;
        this.salary = salary;
        this.effectiveDate = effectiveDate;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Date getEffectiveDate() {
        return this.effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getSalary() {
        return this.salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }
}
