package com.mindex.challenge.data;

/**
 * The ReportingStructure class maintains two properties, an employee and an
 * integer representing the total number of reports under that employee. This
 * was written under the assumption that no cyclic reporting structures exist. 
 * It is assumed that a recursive traversal is sufficient without exceeding stack 
 * limits, and that the direct reports of each employee are non-null.
 */

public class ReportingStructure {
    private Employee employee;
    private int numberOfReports; 

    public ReportingStructure() {
    }
    
    public ReportingStructure(Employee employee) {
        this.employee = employee;
        this.numberOfReports = this.calculateNumberOfReports(employee);
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public int getNumberOfReports() {
        return this.numberOfReports;
    }
    
    public void setNumberOfReports(int numberOfReports) {
        this.numberOfReports = numberOfReports;
    }

    private int calculateNumberOfReports(Employee employee) {
        int sumOfReports = 0;

        if (employee.getDirectReports().isEmpty()) {
            return 0;
        }

        // Iterate over each employee we know as a report and update our sum
        for (Employee e : employee.getDirectReports()) {
            sumOfReports++;
            sumOfReports += this.calculateNumberOfReports(e);
        }
        return sumOfReports;
    }
}   
